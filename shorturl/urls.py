from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^get_short_url$', views.get_short_url, name='get_short_url'),
    url(r'^renewbuy.py/(?P<random_key>[\w-]+)/$', views.renewbuy_py, name='renewbuy_py'),
]
