# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
import json
from shorturl.models import *
import random
from django.shortcuts import redirect


def index(request):
    context = dict()
    return render(request, 'shorturl/index.html', context)


@csrf_protect
def get_short_url(request):
    if request.is_ajax() and request.method == 'POST':
        longurl = request.POST.get("longurl", '')
        if longurl:
            shorturl = "%s" % ''.join(
                random.sample(map(chr, range(48, 57) + range(65, 90) + range(97, 122)), 16))
            obj = ShortUrl(shorturl=shorturl)
            obj2, created = LongUrl.objects.get_or_create(long_url=longurl)
            obj.long_url = obj2
            obj.save()

    result = {"result": shorturl}

    return HttpResponse(json.dumps(result))


def renewbuy_py(request, random_key):
    obj = ShortUrl.objects.filter(shorturl=random_key)
    if obj:
        obj = obj[0]
        longurl = obj.get_long_url().long_url
    else:
        longurl = ''

    result = {"result": longurl}

    return redirect(longurl)
