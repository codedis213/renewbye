# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class LongUrl(models.Model):
    long_url = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.long_url


@python_2_unicode_compatible
class ShortUrl(models.Model):
    long_url = models.ForeignKey(LongUrl, on_delete=models.CASCADE)
    shorturl = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.shorturl

    def get_long_url(self):
        return self.long_url
